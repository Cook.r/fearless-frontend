import React, {useEffect, useState} from 'react';


function ConferenceForm (props) {

  const [name, setName] = useState('');
  const [starts, setStart] = useState('');
  const [ends, setEnd] = useState('');
  const [description, setDescription] = useState('');
  const [max_presentations, setPresentations] = useState('');
  const [max_attendees, setAttendees] = useState('');
  const [locations, setLocations] = useState([]);
  const [location, setLocation] = useState('')

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  }

  const handleStartChange = (event) => {
    const value = event.target.value;
    setStart(value);
  }

  const handleEndChange = (event) => {
    const value = event.target.value;
    setEnd(value);
  }

  const handleDescriptionChange = (event) => {
    const value = event.target.value;
    setDescription(value);
  }
  const handlePresentationChange = (event) => {
    const value = event.target.value;
    setPresentations(value);
  }
  const handleAttendeesChange = (event) => {
    const value = event.target.value;
    setAttendees(value);
  }
  const handleLocationChange = (event) => {
    const value = event.target.value;
    setLocation(value);
  }



  const handleSubmit = async (event) => {
    event.preventDefault();
  
    // create an empty JSON object
    const data = {};
    data.starts = starts;
    data.name = name;
    data.ends = ends;
    data.location = location;
    data.max_presentations = max_presentations
    data.max_attendees = max_attendees
    data.description = description
  
    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    console.log(data)

    const response = await fetch(conferenceUrl, fetchConfig);
    console.log(response)
    if (response.ok) {
      console.log(response)
      const newLocation = await response.json();
      setName('');
      setStart('');
      setEnd('');
      setLocation('')
      setAttendees('')
      setPresentations('')
      setAttendees('')
      setDescription('')
    }
  }

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      console.log(data)
      setLocations(data.locations)
      
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

    return (
      <div className="my-5 container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Name" name="name"  id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStartChange} value={starts} type="date" name="starts"  id="starts" className="form-control"/>
                <label htmlFor="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEndChange} value={ends} type="date" name="ends" id="ends" className="form-control"/>
                <label htmlFor="ends">Ends</label>
              </div>
              <div className="mb-3">
                <label  htmlFor="description" className="form-label">Description</label>
                <textarea onChange={handleDescriptionChange} value={description} name="description" placeholder="description" rows="3" cols="33" id="description" className="form-control">
                </textarea>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePresentationChange} value={max_presentations} type="number" name="max_presentations"  id="max_presentations" className="form-control"/>
                <label htmlFor="description">Max Presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleAttendeesChange} value={max_attendees} type="number" name="max_attendees"  id="max_attendees" className="form-control"/>
                <label htmlFor="max_attendees">Max Attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange} value={location} name="location" required id="location" className="form-select">
                  <option value="">Choose a location</option>
                  {locations.map(conference => {
                    return (
                      <option key={conference.id} value={conference.id}>
                        {conference.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
      );
    }
    export default ConferenceForm 
