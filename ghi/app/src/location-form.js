import React, {useEffect,useState} from 'react';

function LocationForm(props) {

    const [name, setName] = useState('');
    const [room_Count, setroom_Count] = useState('');
    const [city, setCity] = useState('');
    const [state, setState] = useState('');

    const [states, setStates] = useState([]);

    const handleNameChange = (event) => {
      const value = event.target.value;
      setName(value);
    }

    const handleRoomChange = (event) => {
      const value = event.target.value;
      setroom_Count(value);
    }

    const handleCityChange = (event) => {
      const value = event.target.value;
      setCity(value);
    }

    const handleStateChange = (event) => {
      const value = event.target.value;
      setState(value);
    }


    const handleSubmit = async (event) => {
      event.preventDefault();
    
      // create an empty JSON object
      const data = {};
      data.room_count = room_Count;
      data.name = name;
      data.city = city;
      data.state = state;
    
      const locationUrl = 'http://localhost:8000/api/locations/';
      const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };

      const response = await fetch(locationUrl, fetchConfig);
      if (response.ok) {
        const newLocation = await response.json();
        console.log(newLocation);

        setName('');
        setroom_Count('');
        setCity('');
        setState('');
      }
    }


    const fetchData = async () => {
    const url = 'http://localhost:8000/api/states/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      console.log(data)
      setStates(data.states)
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new location</h1>
          <form onSubmit={handleSubmit} id="create-location-form">
            <div className="form-floating mb-3">
              <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name={name} id="name" className="form-control"/>
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleRoomChange} value={room_Count} placeholder="Room count" required type="number" name={room_Count} id="room_count" className="form-control"/>
              <label htmlFor="room_count">Room count</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleCityChange} value={city} placeholder="City" required type="text" name={city} id="city" className="form-control"/>
              <label htmlFor="city">City</label>
            </div>
            <div className="mb-3">
              <select onChange={handleStateChange} required name={state} id="state" className="form-select">
              <option >Choose a state</option>
                  {states.map(state => {
                    return (
                      <option key={state.abb} value={state.abb}>
                        {state.name}
                      </option>
                    );
                  })}
                  </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}
export default LocationForm;
