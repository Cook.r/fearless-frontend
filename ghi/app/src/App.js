import React from 'react';
import './App.css';
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './location-form';
import ConferenceForm from './conference-form';
import AttendeeForm from './attendee-form';
import PresentationForm from './presentation-form';
import MainPage from './MainPage';

import { BrowserRouter, Routes, Route, HashRouter} from "react-router-dom";


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }

  return (
    <BrowserRouter>
      <Nav/>
      <div className="container">
      <Routes>
      <Route path = "/locations">
        <Route path ="new" element ={<LocationForm/>}/>
      </Route>
      <Route path = "/conferences">
        <Route path ="new" element ={<ConferenceForm/>}/>
      </Route>
      <Route path = "/attendees">
        <Route path ="new" element ={<AttendeeForm/>}/>
      </Route>
      <Route path = "/attendees">
        <Route path = "" element ={<AttendeesList attendees={props.attendees}/>}/>
      </Route>
      <Route path = "/presentations">
        <Route path ="new" element ={<PresentationForm/>}/>
      </Route>
      <Route index element={<MainPage />} />
      </Routes>
     {/* <LocationForm /> */}
     {/*<ConferenceForm/> */}
    {/*<AttendeesList attendees={props.attendees}/>*/}
    {/*<AttendeeForm conferences= {props.conferences}/>*/}
    </div>
    </BrowserRouter>
  );
}

export default App;
